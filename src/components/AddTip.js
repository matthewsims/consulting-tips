/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/prop-types */
import React, { useState } from 'react';

const AddTip = (props) => {
  const { handleAddTip } = props;
  const [ tags, setTags ] = useState([]);

  return (
    <>
      <form onSubmit={handleAddTip}>
        <label>
          Tip ID:
          <input type="text" name="tip-id" tabIndex="0" required />
        </label>
        <label>
          Tip Title:
          <input type="text" name="tip-title" tabIndex="0" required />
        </label>
        <label>
          Tip:
          <textarea type="text" name="tip" tabIndex="0" required />
        </label>
        <button type="submit">Add Tip</button>
      </form>
    </>
  );
};

export default AddTip;
