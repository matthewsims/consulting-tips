/* eslint-disable react/prop-types */
import React from 'react';

const Search = (props) => {
  const { handleSearch } = props;

  return (
    <div className="search">
      <input type="search" placeholder="Enter a search term..." tabIndex="0" onChange={handleSearch} />
    </div>
  );
};

export default Search;
