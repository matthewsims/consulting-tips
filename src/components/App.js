/* eslint-disable dot-notation */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as tipActions from '../redux/actions/tipActions';
import Header from './Header';
import Search from './Search';
import Tips from './Tips';
import AddTip from './AddTip';
import { throwError } from './../server/debug';

const App = (props) => {
  // Create the properties in our application's state
  const [ tips, setTip ] = useState({ ...props.tips }); // With "...props.tips", we are adding ONTO props, not overwriting it

  useEffect(() => {
    if (props.tips.length === 0) {
      props.actions.loadTips().catch(error => throwError('./src/components/App.js => useEffect', error));
    }
    else {
      setTip({...props.tips});
    }
  })

  // In this function, we are going to add the functionality of finding tips that relate to our search criteria
  const handleSearch = (e) => {
    try {
      const term = e.target.value; // Current search criteria
    }
    catch (error) {
      throwError('./src/components/App.js => handleSearch', error);
    }
  };

  // With this function, we will add a tip to the application's state, held in the property 'tips'
  const handleAddTip = (e) => {
    e.preventDefault();

    try {
      const tipID = e.target['tip-id'].value; // The Tip ID entered
      const tip = e.target['tip'].value; // The Tip text entered
      const tipFlagged = false; // If the entered tip does not match our requirements, it will be discarded; this Boolean will check for that

      // For all the tips in our state....
      props.tips.forEach(item => {
        // If the entered Tip ID matches an already existing Tip ID...
        if (tipID === item.id) {
          // Display an alert
          alert('This "Tip ID" has already been used; please use a different ID');

          // Flag the tip
          tipFlagged = true;
        }
      });

      // If the tip has a space...
      if (tipID.indexOf(' ') > -1) {
        // Display an alert
        alert('The "Tip ID" cannot have any spaces in it.');

        // Flag the tip
        tipFlagged = true;
      }

      // If the tip has been flagged, discard the tip
      if (tipFlagged) return;

      // If the tip has not been flagged, we will add it to our state
      const newTip = { ...tips, id: tipID, text: tip }; // The first parameter says to add to "tips"
                                                        // The second parameter says to set "tipID" to "id"
                                                        // The third parameter says to set "tip" to "text"
      
      // We will now call our Redux dispatcher to add the tip to our store's state
      // Our dispatcher is in "mapDispatchToProps"
      props.actions.addTip(newTip); // In our Redux actions, we call the "addTip" action with our prospective new tip
      // addTipToDB(inputData);
    }
    catch (error) {
      throwError('./src/components/App.js => handleAddTip', error);
    }
  };

  const addTipToDB = (tip) => {
    try {
      addNewTip(tip);
    }
    catch (error) {
      throwError('./src/components/App.js => addTipToDB', error);
    }
  };

  return (
    <>
      <Header />
      <Search handleSearch={handleSearch} />
      <Tips tips={props.tips} />
      <AddTip handleAddTip={handleAddTip} />
    </>
  );
};

/* 
// We tell the component what properties Redux will expect to pass into our component
App.propTypes = {
  dispatch: PropTypes.func.isRequired,
}; */

// With mapStateToProps, we are hooking up our Redux store's state to this component's props
const mapStateToProps = (state, ownProps) => {
  return {
    tips: state.tips, // Set our component's props to our Redux store's state
  };
};

// With mapDispatchTopProps, we are connecting our dispatcher to our component
const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(tipActions, dispatch) // Creates a relationship between our actions and our dispatcher
  };
};

// This is where the mapStateToProps and mapDispatchToProps meet our component
export default connect(mapStateToProps, mapDispatchToProps)(App);