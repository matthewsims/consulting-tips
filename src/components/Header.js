/* eslint-disable max-len */
import React from 'react';

const Header = () => (
  <header>
    <h1>Consulting Tips</h1>
    <p>Use the search bar below to find consulting tips regarding a specific topic. All consulting tips are shown by default.</p>
  </header>
);

export default Header;
