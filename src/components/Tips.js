/* eslint-disable no-unused-expressions */
/* eslint-disable react/prop-types */
import React from 'react';

const Tips = (props) => {
  const { tips } = props;

  return (
    <div className="tips">
      {Object.values(tips).map(item => <div key={item.id} id={item.id}>{item.text}</div>)}
    </div>
  );
};

export default Tips;
