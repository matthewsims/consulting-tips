import { combineReducers } from 'redux';
import tips from './tipReducer';

// In this reducer, we are centralizing all reducers into this one reducer for organization
const rootReducer = combineReducers({
  tips,
});

export default rootReducer;
