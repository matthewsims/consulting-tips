import * as types from '../actions/actionTypes';

// The "tipReducer" will handle actions related to tips
export default function tipReducer(state = [], action) {  // The first parameter means we will not overwrite the existing state but create a whole new one
                                                          // The second parameter is what action is happening
  switch (action.type) {
    // If we are adding a tip...
    case types.ADD_TIP:
      // Add the new tip to our state; the first parameter is our new state, and the second parameter says to add to the first parameter (i.e. our new state)
      return [...state, { ...action.tip }];
    // If we are loading tips...
    case types.LOAD_TIPS_SUCCESS:
      // Simply return the tips
      return action.tips;
    default:
      return state;
  }
}
