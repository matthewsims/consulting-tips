import { axios } from 'axios';
import * as types from './actionTypes';
import { throwError } from './../../server/debug';

const url = 'http://localhost:7777';

// With this action, we will add a tip to our Redux state's store
export function addTip(tip) {
  return { type: types.ADD_TIP, tip}; // In an action, you must ALWAYS include a "type"; all other parameters are optional
}

// With this action, we have successfully loaded the tips into our Redux state's store
export function loadTipsSuccess(tips) {
  return { type: types.LOAD_TIPS_SUCCESS, tips }; // In an action, you must ALWAYS include a "type"; all other perameters are optional
}

// With this action, we are loading the tips into our Redux state's store
// Calls "loadTipsSuccess"
export function loadTips() {
  // Returns this function
  // NOTE: THIS FORMAT IS NECESSARY
  return async function(disptach) {
    try {
      const collection = {};
      axios.get(url + '/tip/load')
        .then(response => console.log(response))
        .catch(error => throwError('./src/redux/actions/tipActions => loadTips => axios.get', error));
      // Return the Mongo collection using our action "loadTipsSuccess"
      return dispatch(loadTipsSuccess(collecion));
    }
    catch (error) {
      throwError('./src/redux/actions/tipActions => loadTips', error);
    }
  }
}