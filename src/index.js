import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import App from './components/App';
import configureStore from './redux/configureStore';

// This function will spin up our store
const store = configureStore();

ReactDOM.render(
  // We assign our store to the application here
  <Provider store={store}>
    <App />
  </Provider>, 
  document.querySelector('#app')
);
