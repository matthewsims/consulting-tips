import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { connectDB } from './connect-db';
import { throwError } from './debug';

const port = 7777;
const app = express();

app.listen(port, console.log(`Server listening on port ${port}...`));

app.use(
  cors(),
  bodyParser.urlencoded({ extended: true }),
  bodyParser.json()
);

export const addNewTip = async (tip) => {
  try {
    let db = await connectDB();
    let collection = db.collection('tips');
    await collection.insertOne(tip);
  }
  catch (error) {
    throwError('./src/server/server.js => addNewTip', error);
  }
};

export const updateTip = async (tipObj) => {
  try {
    let { id, title, text } = tipObj;
    let db = await connectDB();
    let collection = db.collection('tips');

    if (tip) {
      await collection.updateOne({ id }, { $set: { text } });
    }
  }
  catch (error) {
    throwError('./src/server/server.js => updateTip', error);
  }
};

export const loadInitialTips = async () => {
  try {
  }
  catch (error) {
    throwError('./src/server/server.js => loadInitialTips', error);
  }
};

app.post('/tip/new', async (request, response) => {
  try {
    let task = request.body.tip;
    await addNewTip(tip);
    response.status(200).send();
  }
  catch (error) {
    throwError('./src/server/server.js => app.post(\'/tip/new\')', error);
  }
});

app.post('/tip/update', async (request, response) => {
  try {
    let task = request.body.tip;
    await updateTip(tip);
    response.status(200).send();
  }
  catch (error) {
    throwError('./src/server/server.js => app.post(\'/tip/update\')', error);
  }
});

app.get('/tip/load', async (request, response) => {
  console.log(response)
});
