export const throwError = (filePath, error) => {
  console.error(`There was an error in ${filePath}: ${error}`);
};