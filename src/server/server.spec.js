import { addNewTip, updateTip } from './server';

(async function manageTips() {
  addNewTip({
    id: 'daadfdafads',
    tip: 'dhfjdsfkjdsakjfdnsfdnls hfdfhadks',
  });

  await updateTip({
    id: 'daadfdafads',
    tip: 'I was updated!',
  });
})();
