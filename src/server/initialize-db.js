import { connectDB } from './connect-db';

async function initializeDB() {
  let db = await connectDB();
  let collection = db.collection('tips');
  return collection
}
