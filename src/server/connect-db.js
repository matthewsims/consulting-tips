import { MongoClient } from 'mongodb';
import { throwError } from './debug';

const url = 'mongodb://localhost:27017/consultingtips';
let db = null;

export async function connectDB() {
  if (db) return db;
  
  try {
    let client = await MongoClient.connect(url, { useNewUrlParser: true });
    db = client.db();
    console.info('Got DB', db);
    return db;
  }
  catch (error) {
    throwError('./src/server/connect-db => connectDB', error);
  }
}
